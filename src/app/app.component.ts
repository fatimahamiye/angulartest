import { Component,Inject } from '@angular/core';
import {MatDialog, MatDialogConfig} from "@angular/material";
import { CartComponent } from 'src/app/cart/cart.component';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'AngularTest';
 private loggedin;
 constructor(private dialog: MatDialog) {}

 opencart() {

  const dialogConfig = new MatDialogConfig();
  this.dialog.open(CartComponent, dialogConfig);
    //   console.log("cart");
    //  const dialogConfig = new MatDialogConfig();
 
     dialogConfig.disableClose = true;
     dialogConfig.autoFocus = true;
 
    //  this.dialog.open(CartComponent, dialogConfig);
 }

  ngOnInit() {
    if(window.localStorage.getItem("access_token")) this.loggedin=true;
   }
}
