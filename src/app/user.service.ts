import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/internal/Observable';
import { HttpErrorResponse } from '@angular/common/http/src/response';
//import {Headers} from '@angular/common/http';
import { User, Ilogin } from './user';
import { catchError } from 'rxjs/operators';
import { throwError } from 'rxjs';
import { map } from "rxjs/operators";
//import {RequestOptions, Request, RequestMethod} from '@angular/';
@Injectable({
  providedIn: 'root'
})

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private http:HttpClient) { }

public loginurl="https://admin.konchef.com/api/auth/login";

 loginform(form):Observable<Ilogin> {
  let header = new Headers({ 'Content-Type': 'application/json' });
   console.log(form.value);

   let body = {
    'email': form.value.email,
    'password': form.value.password
};
  console.log(body);
  return this.http.post(this.loginurl,body )
   .pipe(
         map(response =>  (response)),
        catchError(this.handlerror)
    );
}
handlerror(error: HttpErrorResponse){
  return throwError(error.message||"server error");
}

}
