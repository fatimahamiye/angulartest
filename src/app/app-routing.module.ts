import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from 'src/app/login/login.component';
import { AccountComponent } from 'src/app/account/account.component';
import { ItemsComponent } from 'src/app/items/items.component';
import { CartComponent } from 'src/app/cart/cart.component';
import { bloomAdd } from '@angular/core/src/render3/di';
import { BlogComponent } from 'src/app/blog/blog.component';
import { BlogdetailComponent } from 'src/app/blogdetail/blogdetail.component';
import { LogoutComponent } from 'src/app/logout/logout.component';

const routes: Routes = [
  //  {path: '', redirectTo:'/list',pathMatch:'full'},
   {path: 'login', component:LoginComponent},
   {path: 'logout', component:LogoutComponent},
   {path: 'myaccount', component:AccountComponent},
   {path: 'items', component:ItemsComponent},
   {path: 'cart', component:CartComponent},
   {path: 'blog', component:BlogComponent},
   {path: 'blog/:id', component:BlogdetailComponent},


];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
