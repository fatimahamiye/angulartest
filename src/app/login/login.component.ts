import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/user.service';
import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
public token="";

public loggedin=false;
  constructor(private router: Router ,private _userservice: UserService) { }

  ngOnInit() {
   if(window.localStorage.getItem("access_token")) this.loggedin=true;
  }
  login(form){
    this._userservice.loginform(form).subscribe(res => {
     this.token= res.access_token;
     window.localStorage.setItem("access_token", this.token);
     window.localStorage.setItem("email", res.user.email);
     window.localStorage.setItem("name", res.user.name);
     if(res.access_token)this.loggedin=true;

     console.log(this.token);
     window.location.reload();

  });


   // calllogin(form);
  }
}
