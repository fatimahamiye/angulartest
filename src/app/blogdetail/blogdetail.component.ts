import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-blogdetail',
  templateUrl: './blogdetail.component.html',
  styleUrls: ['./blogdetail.component.css']
})
export class BlogdetailComponent implements OnInit {
private blog;
private blogid
  constructor(private _activatedRoute:ActivatedRoute) {

    _activatedRoute.params.subscribe(params => {
      this.blogid = +params['id'];})
      console.log(this.blogid)
   }

  ngOnInit() {
    ///consideer that there is an api that call a specific blog and return a response similare to blogdetail
    this.blog=
                {id: 1, title: 'the title is first',subtext:"text",image:"item.svg",text:"Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum."}
              ;
              }

}
