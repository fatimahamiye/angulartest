import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-account',
  templateUrl: './account.component.html',
  styleUrls: ['./account.component.css']
})
export class AccountComponent implements OnInit {
private email;
private name;

  constructor() { }

  ngOnInit() {
   this.email= window.localStorage.getItem("email");
   this.name= window.localStorage.getItem("name");
  }

}
